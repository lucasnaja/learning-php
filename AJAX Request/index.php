<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Document</title>
    <style>
      html, body {
        background-color: rgb(237, 237, 237);
        margin: 0
      }

      form {
        border: 1px solid rgb(0, 82, 82);
        box-shadow: 0 0 45px darkcyan;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        width: 320px;
        height: 400px;
        background-color: darkcyan
      }

      form input {
        position: relative;
        left: 50%;
        transform: translateX(-50%);
        font-size: 22px;
      }

      form p {
        font-size: 20px;
        margin: 5px 0 0 0;
        text-indent: 15px;
        color: white;
        text-align: left;
      }

      form input[type=button] {
        cursor: pointer;
        font-weight: 800;
        border: 2px solid rgb(0, 82, 82);
        background-color: darkcyan;
        color: white;
        font-size: 24px;
        margin-top: 5px;
        position: relative;
        padding: 10px 20px;
        left: 50%;
        transform: translateX(-50%)
      }

      form input[type=button]:hover { background-color: rgb(0, 156, 156) }

      form h1 {
        color: white;
        margin: 0;
        text-align: center;
      }
    </style>
    
    <script>
      function login(email, password) {
        if (email === '' || password === '')
          document.querySelector('#result').innerText = 'Wrong login!'
        else {
          const XMLRequest = new XMLHttpRequest()
          XMLRequest.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200)
              document.querySelector('#result').innerText = this.responseText
          }

          XMLRequest.open('GET', `login.php?email=${email}&password=${password}`, true)
          XMLRequest.send()
        }
      }
    </script>
  </head>

  <body>
    <form autocomplete="on">
      <p>E-mail</p>
      <input type="email" id="email"/>
      <p>Password</p>
      <input type="password" id="password"/>
      <input title="Login" type="button" value="Login" onclick="login(document.querySelector('#email').value, document.querySelector('#password').value)"/>

      <h1 id="result">Waiting login</h1>
    </form>
  </body>
</html>