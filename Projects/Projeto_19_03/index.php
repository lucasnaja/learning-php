<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="src/index.css">

    <link type="text/css" rel="stylesheet" href="src/materialize.min.css">
    <script type="text/javascript" src="src/materialize.min.js"></script>
</head>

<body>
    <?php
    if (isset($msg)) {
        $msg = $_GET['msg'];

        if ($msg === 'success') {
            echo '<script>M.toast({html: "Cadastro feito com sucesso!",classes: "green"})</script>';
        } else if ($msg === 'error') {
            echo '<script>M.toast({html: "Login não existente!",classes: "red accent-4"})</script>';
        }
    }
    ?>
    <main>
        <div class="container">
            <div class="row">
                <div class="card-panel">
                    <h1 class="flow-text mt-2">Seja bem-vindo, logue com seu e-mail e senha abaixo.</h1>
                    <form action="validar.php" method="post">
                        <div class="input-field">
                            <label>E-mail</label>
                            <input placeholder="E-mail" type="email" name="email" required />
                        </div>

                        <div class="input-field">
                            <label>Senha</label>
                            <input placeholder="Senha" type="password" name="senha" required />
                        </div>
                        <input class="btn grey darken-3" type="submit" value="Logar" />
                        <a class="btn grey darken-3" href="administrador/cadastro.php">Registrar</a>
                    </form>
                </div>
            </div>
        </div>
    </main>

    <footer class="page-footer grey darken-3">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Contato</h5>
                    <p class="grey-text text-lighten-4">
                        Page Title
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/Lucas.Naja0">Facebook</a></li>
                        </li>
                        <li><a class="grey-text text-lighten-3" href="https://twitter.com/LucasNaja0">Twitter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2019 Lucas Bittencourt
            </div>
        </div>
    </footer>
</body>

</html> 