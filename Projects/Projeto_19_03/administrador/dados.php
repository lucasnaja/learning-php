<?php 
session_start();

if (!$_SESSION['Login']) {
    header("Location: ../");
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Meus dados</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../src/index.css">

    <link type="text/css" rel="stylesheet" href="../src/materialize.min.css">
    <script type="text/javascript" src="../src/materialize.min.js"></script>
</head>

<body>
    <nav class="grey darken-3">
        <div class="nav-wrapper">
            <a href="clientes.php" class="brand-logo">Dados</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="inicio.php">Início</a></li>
                <li><a href="sair.php">Sair</a></li>
            </ul>
        </div>
    </nav>

    <ul class="sidenav" id="mobile-demo">
        <li><a href="inicio.php">Início</a></li>
        <li><a href="sair.php">Sair</a></li>
    </ul>

    <main>
        <div class="container">
            <div class="card-panel">
                <h1 class="flow-text mt-2">Dados</h1>

                <table class="centered highlight">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Senha</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php include 'select.php' ?>
                    </tbody>
                </table>
            </div>
        </div>
    </main>

    <footer class="page-footer grey darken-3">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Contato</h5>
                    <p class="grey-text text-lighten-4">
                        Email:
                        <?= $_SESSION['Login']['email'] ?>
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/Lucas.Naja0">Facebook</a></li>
                        </li>
                        <li><a class="grey-text text-lighten-3" href="https://twitter.com/LucasNaja0">Twitter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2019 Lucas Bittencourt
            </div>
        </div>
    </footer>

    <script type="text/javascript">
        M.Sidenav.init(document.querySelectorAll('.sidenav'))
    </script>
</body>

</html> 