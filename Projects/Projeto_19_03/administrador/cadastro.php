<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registrar</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../src/index.css">

    <link type="text/css" rel="stylesheet" href="../src/materialize.min.css">
    <script type="text/javascript" src="../src/materialize.min.js"></script>
</head>

<body>
    <?php
    if (isset($msg)) {
        $msg = $_GET['msg'];
        if ($msg === 'error') {
            echo '<script>M.toast({html: "Algo deu errado!",classes: "red accent-4"})</script>';
        }
    }
    ?>
    <main>
        <div class="container">
            <div class="row">
                <div class="card-panel">
                    <h1 class="flow-text mt-2">Registre seu e-mail e senha abaixo:</h1>
                    <form action="cadastro_users.php" method="post">
                        <div class="input-field">
                            <label>Nome</label>
                            <input placeholder="Coloque seu nome aqui." type="text" name="nome" required>
                        </div>

                        <div class="input-field">
                            <label>E-mail</label>
                            <input placeholder="Coloque seu e-mail aqui." type="email" name="email" required>
                        </div>

                        <div class="input-field">
                            <label>Senha</label>
                            <input placeholder="Coloque sua senha aqui." type="password" name="senha" required>
                        </div>
                        <input class="btn grey darken-3" type="submit" value="Registrar">
                        <a class="btn grey darken-3" href="../">Voltar</a>
                    </form>
                </div>
            </div>
        </div>
    </main>

    <footer class="page-footer grey darken-3">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Contato</h5>
                    <p class="grey-text text-lighten-4">
                        Page Title
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/Lucas.Naja0">Facebook</a></li>
                        </li>
                        <li><a class="grey-text text-lighten-3" href="https://twitter.com/LucasNaja0">Twitter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2019 Lucas Bittencourt
            </div>
        </div>
    </footer>
</body>

</html> 