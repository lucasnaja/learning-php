<?php
try {
    $pdo = new PDO('mysql:host=127.0.0.1;dbname=dbphp2;charset=utf8', 'root', '', []);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $email = filter_input(INPUT_POST, 'email', FILTER_DEFAULT);
    $senha = filter_input(INPUT_POST, 'senha', FILTER_DEFAULT);

    $prep = $pdo->prepare("INSERT INTO usuarios (email, senha) VALUES ('$email', '$senha')");

    if ($prep->execute()) {
        header('Location: ./?msg=success');
    } else {
        header('Location: ./?msg=error');
    }
} catch (PDOException $e) {
    header('Location: ./?msg=error');
}
