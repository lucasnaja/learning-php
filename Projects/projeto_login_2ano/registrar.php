<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registrar</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">

    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css" />
</head>

<body>
    <main>
        <div class="container">
            <div class="row">
                <div class="card-panel">
                    <h6>Registre seu e-mail e senha abaixo:</h6>
                    <form action="register.php" method="post">
                        <div class="input-field">
                            <label>E-mail</label>
                            <input type="email" name="email" required />
                        </div>

                        <div class="input-field">
                            <label>Senha</label>
                            <input type="password" name="senha" required />
                        </div>
                        <input class="btn indigo darken-4" type="submit" value="Registrar" />
                    </form>
                </div>
            </div>
        </div>
    </main>

    <footer class="page-footer indigo darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Contato</h5>
                    <p class="grey-text text-lighten-4">
                        Page Title
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/Lucas.Naja0">Facebook</a></li>
                        </li>
                        <li><a class="grey-text text-lighten-3" href="https://twitter.com/LucasNaja0">Twitter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2019 Lucas Bittencourt
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
</body>

</html> 