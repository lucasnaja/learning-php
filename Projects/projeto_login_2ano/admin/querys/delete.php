<?php
try {
    $pdo = new PDO('mysql:host=127.0.0.1;dbname=dbphp2;charset=utf8', 'root', '', []);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $id = $_GET['id'];

    $prep = $pdo->prepare("DELETE FROM clientes WHERE id=$id");

    if ($prep->execute()) {
        header('Location: ../clientes.php?msg=deletado');
    } else {
        header('Location: ../clientes.php?msg=naoDeletado');
    }
} catch (PDOException $e) {
    header('Location: ../clientes.php?msg=naoDeletado');
}

