<?php
try {
    $pdo = new PDO('mysql:host=127.0.0.1;dbname=dbphp2;charset=utf8', 'root', '', []);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $nome = filter_input(INPUT_POST, 'nome', FILTER_DEFAULT);
    $sobrenome = filter_input(INPUT_POST, 'sobrenome', FILTER_DEFAULT);

    $prep = $pdo->prepare("INSERT INTO clientes (nome, sobrenome) VALUES ('$nome', '$sobrenome')");
    
    if ($prep->execute()) {
        header('Location: ../clientes.php?msg=cadastrado');
    } else {
        header('Location: ../clientes.php?msg=naoCadastrado');
    }

} catch (PDOException $e) {
    header('Location: ./clientes.php?msg=naoCadastrado');
}
