<?php
try {
    $pdo = new PDO('mysql:host=127.0.0.1;dbname=dbphp2;charset=utf8', 'root', '', []);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $email = filter_input(INPUT_POST, 'email', FILTER_DEFAULT);
    $senha = filter_input(INPUT_POST, 'senha', FILTER_DEFAULT);

    $prep = $pdo->prepare("SELECT * FROM clientes");

    if ($prep->execute()) {
        $data = $prep->fetchAll();
    }

    foreach ($data as $key) {
        echo '<tr><td>';
        echo $key['nome'];
        echo '</td><td>';
        echo $key['sobrenome'];
        echo '</td><td><a href="querys/delete.php?id='.$key['id'].'"><i class="material-icons red-text">clear</i></td></a></tr>';
    }
} catch (PDOException $e) {
    echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
