<?php 
session_start();

if (!$_SESSION['Login']) {
    header("Location: ../index.php");
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Título</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/style.css">

    <!--Import MATERIALIZE.CSS-->
    <link type="text/css" rel="stylesheet" href="../materialize/css/materialize.min.css" />
</head>

<body>
    <nav class="indigo darken-4">
        <div class="nav-wrapper">
            <a href="#!" class="brand-logo">Home</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="clientes.php">Clientes</a></li>
                <li><a href="funcionarios.php">Funcionários</a></li>
                <li><a href="usuarios.php">Usuários</a></li>
                <li><a href="sair.php">Sair</a></li>
            </ul>
        </div>
    </nav>

    <ul class="sidenav" id="mobile-demo">
        <li><a href="clientes.php">Clientes</a></li>
        <li><a href="funcionarios.php">Funcionários</a></li>
        <li><a href="usuarios.php">Usuários</a></li>
        <li><a href="sair.php">Sair</a></li>
    </ul>

    <main>
        <ul class="collapsible">
            <li>
                <div class="collapsible-header">
                    <i class="material-icons">filter_drama</i>
                    <?= $_SESSION['Login']['email']; ?>
                    <span class="new badge indigo">2</span>
                </div>
                <div class="collapsible-body">
                    <p>Lorem ipsum dolor sit amet.</p>
                    <p>Lorem ipsum dolor sit amet.</p>
                </div>
            </li>
        </ul>
    </main>

    <footer class="page-footer indigo darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Contato</h5>
                    <p class="grey-text text-lighten-4">
                        Email:
                        <?= $_SESSION['Login']['email'] ?>
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/Lucas.Naja0">Facebook</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://twitter.com/LucasNaja0">Twitter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2019 Lucas Bittencourt
            </div>
        </div>
    </footer>

    <!--Import MATERIALIZE.JS-->
    <script src="../materialize/js/materialize.min.js"></script>

    <script>
        M.Sidenav.init(document.querySelectorAll('.sidenav'))
        M.Collapsible.init(document.querySelectorAll('.collapsible'))
    </script>
</body>

</htm l> 