<?php 
session_start();

if (!$_SESSION['Login']) {
    header("Location: ../index.php");
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/style.css">

    <!--Import MATERIALIZE.CSS-->
    <link type="text/css" rel="stylesheet" href="../materialize/css/materialize.min.css" media="screen,projection" />
</head>

<body>

    <nav class="indigo darken-4">
        <div class="nav-wrapper">
            <a href="home.php" class="brand-logo">Home</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="clientes.php">Clientes</a></li>
                <li><a href="funcionarios.php">Funcionários</a></li>
                <li><a href="usuarios.php">Usuários</a></li>
                <li><a href="sair.php">Sair</a></li>
            </ul>
        </div>
    </nav>

    <ul class="sidenav" id="mobile-demo">
        <li><a href="clientes.php">Clientes</a></li>
        <li><a href="funcionarios.php">Funcionários</a></li>
        <li><a href="usuarios.php">Usuários</a></li>
        <li><a href="sair.php">Sair</a></li>
    </ul>

    <main>
        <div class="container col s12 col md8">
            <table class="centered highlight responsive-table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Função</th>
                        <th>Sálario</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>Vinicius</td>
                        <td>Faxineiro</td>
                        <td>R$1.200,00</td>
                    </tr>
                    <tr>
                        <td>Marcelo</td>
                        <td>Frentista</td>
                        <td>R$1.250,00</td>
                    </tr>
                    <tr>
                        <td>Renan</td>
                        <td>Porteiro</td>
                        <td>R$1.100,00</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </main>

    <footer class="page-footer indigo darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Contato</h5>
                    <p class="grey-text text-lighten-4">
                        Email:
                        <?= $_SESSION['Login']['email'] ?>
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/LuizGuilhermeJose">Facebook</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://www.instagram.com/luizguilhermejrm/">Instagram</a>
                        </li>
                        <li><a class="grey-text text-lighten-3" href="https://twitter.com/LuizGuilhermeJ">Twitter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2019 Lucas Bittencourt
            </div>
        </div>
    </footer>

    <!--Import MATERIALIZE.JS-->
    <script type="text/javascript" src="../materialize/js/materialize.min.js"></script>

    <script type="text/javascript">
        M.Sidenav.init(document.querySelectorAll('.sidenav'))
    </script>
</body>

</htm l> 