<?php
try {
   include '../../conexao.php';

   $prep = $pdo->prepare("SELECT * FROM medics");

   if ($prep->execute()) {
      $data = $prep->fetchAll();
   }

   foreach ($data as $key) {
      echo '<tr><td>';
      echo $key['med_name'];
      echo '</td><td>';
      echo $key['med_email'];
      echo '</td><td><a href="delete_medicos.php?use_id=' . $key['med_id'] . '"><i class="material-icons red-text">clear</i></td></a>';
      echo '<td><a href="#!"><i class="material-icons green-text">refresh</i></a></td></tr>';
   }
} catch (PDOException $e) {
   echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
