<?php
session_start();

if (!isset($_SESSION['Login'])) {
   header("Location: ..");
}
?>
<!DOCTYPE html>
<html>

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Pacientes</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="../../src/index.css">
   <link type="text/css" rel="stylesheet" href="../../src/materialize.min.css">
   <style>
      .no-bg {
         background: 0
      }
   </style>
</head>

<body>
   <nav class="indigo darken-4">
      <div class="nav-wrapper">
         <a href="clientes.php" class="brand-logo">Usuários</a>
         <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
         <ul class="right hide-on-med-and-down">
            <li><a href="../home.php">Home</a></li>
            <li><a href="../agenda/form_agenda.php">Agenda</a></li>
            <li><a href="../medicos/form_medicos.php">Médicos</a></li>
            <li class="active"><a href="form_pacientes.php">Pacientes</a></li>
            <li><a href="../usuarios/form_usuarios.php">Usuários</a></li>
            <li><a href="../exit.php">Sair</a></li>
         </ul>
      </div>
   </nav>

   <ul class="sidenav" id="mobile-demo">
      <li><a href="../home.php">Home</a></li>
      <li><a href="../agenda/form_agenda.php">Agenda</a></li>
      <li><a href="../medicos/form_medicos.php">Médicos</a></li>
      <li class="active"><a href="form_pacientes.php">Pacientes</a></li>
      <li><a href="../usuarios/form_usuarios.php">Usuários</a></li>
      <li><a href="../exit.php">Sair</a></li>
   </ul>

   <main>
      <div class="container">
         <div class="card-panel" style="margin-top:25px">
            <h1 class="flow-text" style="margin-top:5px">Registrar Paciente</h1>
            <form action="insert_pacientes.php" method="post">
               <div class="row mb-0">
                  <div class="input-field col s12">
                     <label>Nome</label>
                     <input placeholder="Nome" id="nome" type="text" name="nome" class="validate" oninvalid="this.setCustomValidity('Preencha esse campo com o nome do usuário.')" oninput="setCustomValidity('')" required>
                  </div>

                  <div class="input-field col s12">
                     <label>E-mail</label>
                     <input placeholder="E-mail" id="email" type="email" name="email" class="validate" oninvalid="this.setCustomValidity('Preencha esse campo com seu e-mail.')" oninput="setCustomValidity('')" required>
                  </div>

                  <div class="input-field col s12">
                     <label>Senha</label>
                     <input placeholder="Senha" id="senha" type="password" name="senha" class="validate" oninvalid="this.setCustomValidity('Preencha esse campo com sua senha.')" oninput="setCustomValidity('')" required>
                  </div>
               </div>

               <input class="btn indigo darken-4" type="submit" value="Registrar" />
            </form>

            <h2 class="flow-text">Usuários</h2>
            <table class="centered highlight">
               <thead>
                  <tr>
                     <th>Nome</th>
                     <th>E-mail</th>
                     <th>Remover</th>
                     <th>Alterar</th>
                  </tr>
               </thead>

               <tbody>
                  <?php include 'select_pacientes.php' ?>
               </tbody>
            </table>
         </div>
      </div>
   </main>

   <footer class="page-footer indigo darken-4">
      <div class="container">
         <div class="row">
            <div class="col l6 s12">
               <h5 class="white-text">Contato</h5>
               <p class="grey-text text-lighten-4">
                  Email: <?= $_SESSION['Login']['email'] ?>
               </p>
               <p class="grey-text text-lighten-4">
                  Usuário: <?= $_SESSION['Login']['name'] ?>
               </p>
            </div>
            <div class="col l4 offset-l2 s12">
               <h5 class="white-text">Links</h5>
               <ul>
                  <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/Lucas.Naja0">Facebook</a></li>
                  </li>
                  <li><a class="grey-text text-lighten-3" href="https://twitter.com/LucasNaja0">Twitter</a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <div class="footer-copyright">
         <div class="container">
            © 2019 Lucas Bittencourt
         </div>
      </div>
   </footer>

   <script type="text/javascript" src="../../src/materialize.min.js"></script>
   <script type="text/javascript">
      M.Sidenav.init(document.querySelectorAll('.sidenav'))
   </script>
</body>

</html>