<?php
try {
   include '../../conexao.php';

   $nome = filter_input(INPUT_POST, 'nome', FILTER_DEFAULT);
   $email = filter_input(INPUT_POST, 'email', FILTER_DEFAULT);
   $senha = filter_input(INPUT_POST, 'senha', FILTER_DEFAULT);

   $prep = $pdo->prepare("INSERT INTO pacients (pac_name, pac_email, pac_password) VALUES (:nome, :email, :senha)");

   $prep->bindValue(':nome', $nome);
   $prep->bindValue(':email', $email);
   $prep->bindValue(':senha', MD5($senha));

   $prep->execute();
   header('Location: form_pacientes.php');
} catch (PDOException $e) {
   echo $e->getMessage();
}
