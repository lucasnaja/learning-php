<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../src/index.css">

    <link type="text/css" rel="stylesheet" href="../src/materialize.min.css">
</head>

<body>
    <nav class="indigo darken-4">
        <div class="nav-wrapper">
            <a href="cadastro.php" class="brand-logo center">Registro</a>
        </div>
    </nav>

    <main>
        <div class="container">
            <div class="card-panel">
                <h6>Página de Registro</h6>
                <form action="cadastro_users.php" method="post">
                    <div class="row">
                        <div class="input-field col s12 m-0">
                            <label>Nome</label>
                            <input class="validate" type="text" name="nome" oninvalid="this.setCustomValidity('Por favor, preencha esse campo com seu nome.')" oninput="setCustomValidity('')" required>
                        </div>

                        <div class="input-field col s12 m-0">
                            <label>E-mail</label>
                            <input class="validate" type="email" name="email" oninvalid="this.setCustomValidity('Por favor, preencha esse campo com seu e-mail.')" oninput="setCustomValidity('')" required>
                        </div>

                        <div class="input-field col s12 m6 m-0">
                            <label>Senha</label>
                            <input class="validate" type="password" name="senha" oninvalid="this.setCustomValidity('Por favor, preencha esse campo com sua senha.')" oninput="setCustomValidity('')" required>
                        </div>

                        <div class="input-field col s12 m6 m-0">
                            <label>Repita a senha</label>
                            <input class="validate" type="password" name="newSenha" oninvalid="this.setCustomValidity('Por favor, preencha esse campo com sua senha.')" oninput="setCustomValidity('')" required>
                        </div>
                        <input class="btn indigo darken-4" type="submit" value="Registrar">
                        <a class="btn indigo darken-4" href="../">Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </main>

    <footer class="page-footer indigo darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Contato</h5>
                    <p class="grey-text text-lighten-4">
                        Prova PHP - Nelci@Etec 2019
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/Lucas.Naja0">Facebook</a></li>
                        </li>
                        <li><a class="grey-text text-lighten-3" href="https://twitter.com/LucasNaja0">Twitter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2019 Lucas Bittencourt
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="../src/materialize.min.js"></script>
</body>

</html>