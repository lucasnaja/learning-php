<?php
try {
    include '../../conexao.php';

    $id = filter_input(INPUT_POST, 'use_id', FILTER_DEFAULT);
    $name = filter_input(INPUT_POST, 'use_name', FILTER_DEFAULT);
    $email = filter_input(INPUT_POST, 'use_email', FILTER_DEFAULT);

    $prep = $pdo->prepare("UPDATE users SET `use_name`=:nome, `use_email`=:email WHERE `use_id`=:id");

    $prep->bindValue('nome', $name);
    $prep->bindValue('email', $email);
    $prep->bindValue('id', $id);

    $prep->execute();

    header('Location: form_usuarios.php');
} catch (PDOException $e) {
    echo $e->getMessage();
}
