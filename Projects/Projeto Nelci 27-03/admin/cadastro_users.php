<?php
try {
    include '../conexao.php';

    $nome = filter_input(INPUT_POST, 'nome', FILTER_DEFAULT);
    $email = filter_input(INPUT_POST, 'email', FILTER_DEFAULT);
    $senha = base64_encode(filter_input(INPUT_POST, 'senha', FILTER_DEFAULT));

    $prep = $pdo->prepare("INSERT INTO usuarios (usu_email, usu_senha, usu_nome) VALUES ('$email', '$senha', '$nome')");

    if ($prep->execute()) {
        header('Location: ../');
    } else {
        header('Location: cadastro.php');
    }
} catch (PDOException $e) {
    header('Location: cadastro.php');
}
