<?php 
session_start();

if (!$_SESSION['Login']) {
    header("Location: ../../index.php");
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Clientes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../src/index.css">

    <link type="text/css" rel="stylesheet" href="../../src/materialize.min.css" />
    <script type="text/javascript" src="../../src/materialize.min.js"></script>
</head>

<body>
    <nav class="grey darken-3">
        <div class="nav-wrapper">
            <a href="clientes.php" class="brand-logo">Clientes</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="../fornecedores/form_fornecedores.php">Fornecedores</a></li>
                <li><a href="../sair.php">Sair</a></li>
            </ul>
        </div>
    </nav>

    <ul class="sidenav" id="mobile-demo">
        <li><a href="../fornecedores/form_fornecedores.php">Fornecedores</a></li>
        <li><a href="../sair.php">Sair</a></li>
    </ul>

    <main>
        <div class="container">
            <div class="card-panel" style="margin-top:25px">
                <h1 class="flow-text" style="margin-top:5px">Registrar cliente</h1>
                <form action="insert_clientes.php" method="post">
                    <div class="input-field">
                        <label>Nome</label>
                        <input placeholder="Nome" id="nome" type="text" name="nome" class="validate" required>
                    </div>

                    <div class="input-field">
                        <label>Sobrenome</label>
                        <input placeholder="Sobrenome" id="sobrenome" type="text" name="sobrenome" class="validate" required>
                    </div>

                    <input class="btn grey darken-3" type="submit" value="Registrar" />
                </form>

                <h2 class="flow-text">Clientes</h2>
                <table class="centered highlight">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Sobrenome</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php include 'select_clientes.php' ?>
                    </tbody>
                </table>
            </div>
        </div>
    </main>

    <footer class="page-footer grey darken-3">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Contato</h5>
                    <p class="grey-text text-lighten-4">
                        Email:
                        <?= $_SESSION['Login']['email'] ?>
                    </p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="https://www.facebook.com/Lucas.Naja0">Facebook</a></li>
                        </li>
                        <li><a class="grey-text text-lighten-3" href="https://twitter.com/LucasNaja0">Twitter</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2019 Lucas Bittencourt
            </div>
        </div>
    </footer>

    <script type="text/javascript">
        M.Sidenav.init(document.querySelectorAll('.sidenav'))
    </script>
</body>

</html> 