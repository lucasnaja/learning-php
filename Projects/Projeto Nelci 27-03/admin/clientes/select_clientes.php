<?php
try {
    include '../../conexao.php';

    $prep = $pdo->prepare("SELECT * FROM clientes");

    if ($prep->execute()) {
        $data = $prep->fetchAll();
    }

    foreach ($data as $key) {
        echo '<tr><td>';
        echo $key['cli_id'];
        echo '</td><td>';
        echo $key['cli_nome'];
        echo '</td><td>';
        echo $key['cli_sobrenome'];
        echo '</td></tr>';
    }
} catch (PDOException $e) {
    echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
