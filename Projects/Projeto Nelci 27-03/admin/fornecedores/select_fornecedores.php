<?php
try {
    include '../../conexao.php';

    $prep = $pdo->prepare("SELECT * FROM fornecedores");

    if ($prep->execute()) {
        $data = $prep->fetchAll();
    }

    foreach ($data as $key) {
        echo '<tr><td>';
        echo $key['for_id'];
        echo '</td><td>';
        echo $key['for_nome'];
        echo '</td><td>';
        echo $key['for_sobrenome'];
        echo '</td></tr>';
    }
} catch (PDOException $e) {
    echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
