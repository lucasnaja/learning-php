<?php
try {
    include 'conexao.php';

    $email = filter_input(INPUT_POST, 'email', FILTER_DEFAULT);
    $senha = filter_input(INPUT_POST, 'senha', FILTER_DEFAULT);

    $prep = $pdo->prepare("SELECT * FROM usuarios WHERE usu_email=\"$email\" and usu_senha=\"" . base64_encode($senha) . "\"");

    if ($prep->execute()) {
        $data = $prep->fetchAll();
    }

    if ($data[0]['usu_email'] == $email && base64_decode($data[0]['usu_senha']) == $senha) {
        session_start();
        $_SESSION['Login']['email'] = $email;
        $_SESSION['Login']['senha'] = $senha;

        header('Location: admin/clientes/form_clientes.php');
    } else {
        header('Location: .');
    }
} catch (Exception $ex) {
    header('Location: .');
}
