<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Document</title>
	</head>

	<body>
		<?php
			$seconds = 200000;

			$days = (int)($seconds / 86400);
			$seconds -= $days * 86400;

			$hours = (int)($seconds / 3600);
			$seconds -= $hours * 3600;

			$minutes = (int)($seconds / 60);
			$seconds -= $minutes * 60;

			echo "Days: $days<br />Hours: $hours<br />Minutes: $minutes<br /> Seconds: $seconds"
		?>
	</body>
</html>