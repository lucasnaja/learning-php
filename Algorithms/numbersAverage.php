<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>

  <body>
    <?php
      function average(...$numbers) {
        $sum = 0;
        foreach ($numbers as $number)
          $sum += $number;

        return $sum / count($numbers);
      }
    
      echo average(10, 5, 10, 10, 10, 1, 2, 3, 10, 10, 10)
    ?>
  </body>
</html>