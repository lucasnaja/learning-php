<?php
  try {
    # Insert multi values
    $stmt = $conn->prepare('SELECT * FROM user');
    $stmt->execute();
    $result = $stmt->fetchAll();

    if (count($result) === 0) {
      $stmt = $conn->prepare(
        'INSERT INTO user (ID, name, birth, origin) VALUES (DEFAULT, :name, :birth, :origin)'
      );

      $stmt->bindParam(':name', $name);
      $stmt->bindParam(':birth', $birth);
      $stmt->bindParam(':origin', $origin);

      $name = 'Lucas';
      $birth = '1999/07/30';
      $origin = 'Brasil';
      $stmt->execute();

      $name = 'João';
      $birth = '1989/09/13';
      $origin = 'México';
      $stmt->execute();

      $name = 'Maria';
      $birth = '2000/10/23';
      $origin = 'Estados Unidos';
      $stmt->execute();
    }
  } catch (PDOException $e) {
    echo $e->getMessage();
  }