<?php
  try {
    $conn = new PDO("mysql:host=$serverName;dbname=$dbName", $userName, $password);

    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    # Create table
    $conn->exec('
      CREATE TABLE IF NOT EXISTS user(
      ID INT UNSIGNED auto_increment PRIMARY KEY,
      name VARCHAR(25) NOT NULL,
      birth DATE,
      origin VARCHAR(20) DEFAULT "Brasil"
      ) CHARSET=utf8
    ');
  } catch (PDOException $e) {
    echo $e->getMessage();
  }