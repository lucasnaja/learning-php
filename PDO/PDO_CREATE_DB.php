<?php
  $serverName = '127.0.0.1';
  $userName = 'root';
  $password = '';
  $dbName = 'bancophp';

  try {
    $conn = new PDO("mysql:host=$serverName", $userName, $password);

    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    # Create DB
    $conn->exec("CREATE DATABASE IF NOT EXISTS $dbName DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");
  } catch (PDOException $e) {
    echo $e->getMessage();
  }