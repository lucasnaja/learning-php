<?php
  try {
    $stmt = $conn->prepare('SELECT * FROM user');
    $stmt->execute();

    $result = $stmt->fetchAll();

    for ($i = 0; $i < count($result); $i++) {
      echo $result[$i]['name'].' - ';
      echo $result[$i]['birth'].' - ';
      echo $result[$i]['origin'].'<br />';
    }

  } catch (PDOException $e) {
    echo $e->getMessage();
  }