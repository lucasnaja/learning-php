<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  
  <body>
    <?php header("Content-type: text/html; charset=utf-8"); ?>
    <?php require_once('PDO_CREATE_DB.php') ?>
    <?php require_once('PDO_CREATE_TABLE.php') ?>
    <?php require_once('PDO_INSERT_INTO.php') ?>  
    <?php require_once('PDO_SELECT_FROM.php') ?>  
  </body>
</html>